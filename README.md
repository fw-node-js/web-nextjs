## Installation

First, run the development server:

1. Install [Node.js](https://nodejs.org/)
2. Clone this repository.
```
git clone https://gitlab.com/fw-node-js/web-nextjs.git
cd web-nextjs
```
3. Install the dependencies.
```
yarn install
```

3. Create .env file.
```
NEXT_PUBLIC_APPNAME=pawaris.s
NEXT_PUBLIC_MODE=light
NEXT_PUBLIC_CONTENTWIDTH=boxed
NEXT_PUBLIC_FONTFAMILY=Source Sans Pro
```

### Running development
Run this command from inside web-nextjs to open a local web server dev
```
yarn run dev
```
Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

### Running build
1. Build package
```
yarn run build
```
2. Running local web server
```
yarn start
```
3. Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.


## Learn More

To learn more about Next.js, take a look at the following resources:

- [Next.js Documentation](https://nextjs.org/docs) - learn about Next.js features and API.
- [Learn Next.js](https://nextjs.org/learn) - an interactive Next.js tutorial.

You can check out [the Next.js GitHub repository](https://github.com/vercel/next.js/) - your feedback and contributions are welcome!
