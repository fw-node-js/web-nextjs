import { CssBaseline, GlobalStyles } from "@mui/material";
import { ThemeProvider, createTheme } from "@mui/material/styles";
import GlobalStyling from "./globalStyles";
import Overrides from "./overrides";
import themeOption from "./themeOption";

const Theme = (props) => {
  const { settings, children } = props;
  const coreThemeConfig = themeOption(settings);
  let theme = createTheme(coreThemeConfig);
  theme = createTheme(theme, {
    components: { ...Overrides({settings,theme})["components"] },
  });
  return (
    <ThemeProvider theme={theme}>
      <CssBaseline />
      <GlobalStyles styles={() => GlobalStyling(theme)} />
      {children}
    </ThemeProvider>
  );
};
export default Theme;
