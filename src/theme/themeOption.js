import { deepmerge } from "@mui/utils";
import Palette from "./palette";
// import breakpoints from './breakpoints'


const themeOption = (settings) => {
  const themeConfig = {
    palette: Palette(settings.mode, settings.fontFamily),
    // breakpoints: breakpoints(),
  };
  return deepmerge(themeConfig, {
    palette: {
      primary: {
        ...themeConfig.palette.primary,
      },
    },
  });
};
export default themeOption;