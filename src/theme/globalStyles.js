const GlobalStyling = (theme) => {
  return {
    a: {
      color: "inherit",
      textDecoration: "none",
    },
    ".ps__rail-y": {
      zIndex: 1,
      right: "0 !important",
      left: "auto !important",
      "&:hover, &:focus, &.ps--clicking": {
        backgroundColor:
          theme.palette.mode === "light"
            ? "#E4E5EB !important"
            : "#423D5D !important",
      },
      "& .ps__thumb-y": {
        right: "3px !important",
        left: "auto !important",
        backgroundColor:
          theme.palette.mode === "light"
            ? "#C2C4D1 !important"
            : "#504B6D !important",
      },
      ".layout-vertical-nav &": {
        "& .ps__thumb-y": {
          width: 4,
          backgroundColor:
            theme.palette.mode === "light"
              ? "#C2C4D1 !important"
              : "#504B6D !important",
        },
        "&:hover, &:focus, &.ps--clicking": {
          backgroundColor: "transparent !important",
          "& .ps__thumb-y": {
            width: 6,
          },
        },
      },
    },
    "#nprogress": {
      pointerEvents: "none",
      "& .bar": {
        left: 0,
        top: 0,
        height: 3,
        width: "100%",
        zIndex: 2000,
        position: "fixed",
        backgroundColor: theme.palette.primary.main,
      },
    },
    "*::-webkit-scrollbar": {
      width: "10px",
      height: "10px",
    },
    "*::-webkit-scrollbar-track": {
      background: "rgba(0,0,0,0.03)",
    },
    "*::-webkit-scrollbar-thumb": {
      background: "rgba(0,0,0,0.25)",
      borderRadius: 5,
    },
    "*::-webkit-scrollbar-thumb:hover": {
      background: "#555",
    },
    "*::-webkit-calendar-picker-indicator": {
      padding: 0,
      margin: 0,
    },
    "*::-webkit-datetime-edit-day-field": {
      // color: 'green',
    },
    "*::-webkit-datetime-edit-month-field": {
      // color: 'blue',
    },
    "*::-webkit-datetime-edit-year-field": {
      // color: 'red',
    },
  };
};

export default GlobalStyling;
