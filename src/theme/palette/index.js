const Palette = (mode, fontFamily) => {
  const lightColor = "#D2DBDD";
  const darkColor = "#212222";
  const mainColor = mode === "dark" ? darkColor : lightColor;
  return {
    customColors: {
        main: mainColor,
        tableHeaderBg: mainColor,
    },
    common: {
      black: "#000",
      white: "#FFF",
    },
    mode: mode,
    primary: {
      dark: "#2F2E5E",
      light: "#2F2E5E",
      main: "#2F2E5E",
      contrastText: "#FFF",
    },
    secondary: {
      light: "#6B3054",
      main: "#6B3054",
      dark: "#6B3054",
      contrastText: "#FFF",
    },
    success: {
      light: "#95A9AE",
      main: "#95A9AE",
      dark: "#95A9AE",
      contrastText: "#FFF",
    },
    error: {
      light: "#DE6220",
      main: "#DE6220",
      dark: "#DE6220",
      contrastText: "#FFF",
    },
    warning: {
      light: "#F4C993",
      main: "#F4C993",
      dark: "#F4C993",
      contrastText: "#FFF",
    },
    info: {
      light: "#F9FCFB",
      main: "#F9FCFB",
      dark: "#F9FCFB",
      contrastText: "#FFF",
    },
    // grey: {
    //   50: "#FAFAFA",
    //   100: "#F5F5F5",
    //   200: "#EEEEEE",
    //   300: "#E0E0E0",
    //   400: "#BDBDBD",
    //   500: "#9E9E9E",
    //   600: "#757575",
    //   700: "#616161",
    //   800: "#424242",
    //   900: "#212121",
    //   A100: "#D5D5D5",
    //   A200: "#AAAAAA",
    //   A400: "#616161",
    //   A700: "#303030",
    // },
    text: {
      primary: `rgba(${mainColor}, 0.87)`,
      secondary: `rgba(${mainColor}, 0.68)`,
      disabled: `rgba(${mainColor}, 0.38)`,
    },
    divider: `rgba(${mainColor}, 0.12)`,
    background: {
      //   paper: mode === "light" ? "#FFF" : "#312D4B",
      default: mainColor,
      //   paper: mode === "light" ? lightColor : darkColor,
    },
    action: {
      active: `rgba(${mainColor}, 0.54)`,
      hover: `rgba(${mainColor}, 0.04)`,
      selected: `rgba(${mainColor}, 0.08)`,
      disabled: `rgba(${mainColor}, 0.3)`,
      disabledBackground: `rgba(${mainColor}, 0.18)`,
      focus: `rgba(${mainColor}, 0.12)`,
    },
  };
};

export default Palette;
