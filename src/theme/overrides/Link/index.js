import { createTheme } from "@mui/material";

const MuiLink = ({ settings, theme }) => {
  const override = createTheme({
    components: {
      MuiLink: {
        styleOverrides: {
          root: {
            
          },
        },
      },
    },
  });
  return override;
};
export default MuiLink;
