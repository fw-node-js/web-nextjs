import { createTheme } from "@mui/material";

const MuiTypography = ({settings,theme}) => {
  const override = createTheme({
    components: {
      MuiTypography: {
        styleOverrides: {
          root: {
            fontFamily: process.env.NEXT_PUBLIC_FONTFAMILY,
          },
        },
      },
    },
  });
  return override;
};
export default MuiTypography;
