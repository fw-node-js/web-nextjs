import { createTheme } from "@mui/material";

const MuiPaper = ({ settings, theme }) => {
  const override = createTheme({
    components: {
      MuiPaper: {
        styleOverrides: {
          root: {},
        },
      },
    },
  });
  return override;
};
export default MuiPaper;
