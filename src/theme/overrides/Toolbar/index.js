import { createTheme } from "@mui/material";

const MuiToolbar = ({ settings, theme }) => {
  const override = createTheme({
    components: {
      MuiToolbar: {
        styleOverrides: {
          root: {
            color: "#000",
          },
          gutters: {
            "@media (min-width: 0px)": {
              minHeight: 0,
              height: 35,
              lineHeight: 35,
              paddingLeft: 1,
              paddingRight: 1,
            },
          },
        },
      },
    },
  });
  return override;
};
export default MuiToolbar;
