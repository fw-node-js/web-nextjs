import { createTheme } from "@mui/material";

const MuiAppBar = ({settings,theme}) => {
  const override = createTheme({
    components: {
      MuiAppBar: {
        styleOverrides: {
          root: {
            backgroundColor:"rgba(0,0,0,0)",
            boxShadow:'none',
            marginBottom:10
          },
        },
      },
    },
  });
  return override;
};
export default MuiAppBar;
