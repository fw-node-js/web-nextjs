import { createTheme } from "@mui/material";

const MuiListItemText = ({ settings, theme }) => {
  const override = createTheme({
    components: {
      MuiListItemText: {
        styleOverrides: {
          root: {
            whiteSpace: "nowrap",
          },
        },
      },
    },
  });
  return override;
};
export default MuiListItemText;
