import { createTheme } from "@mui/material/styles";
import MuiAppBar from "./AppBar";
import MuiCardContent from "./CardContent";
import MuiLink from "./Link";
import MuiListItem from "./ListItem";
import MuiListItemText from "./ListItemText";
import MuiPaper from "./Paper";
import MuiToolbar from "./Toolbar";
import MuiTypography from "./Typography";

const Overrides = (props) => {
  const overrides = createTheme(
    { ...MuiAppBar(props) },
    { ...MuiCardContent(props) },
    { ...MuiLink(props) },
    { ...MuiListItem(props) },
    { ...MuiListItemText(props) },
    { ...MuiPaper(props) },
    { ...MuiToolbar(props) },
    { ...MuiTypography(props) }
  );
  return overrides;
};
export default Overrides;
