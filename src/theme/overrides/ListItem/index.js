import { createTheme } from "@mui/material";

const MuiListItem = ({ settings, theme }) => {
  const override = createTheme({
    components: {
      MuiListItem: {
        styleOverrides: {
          root: {
            padding:"0 10px 0 10px"
          },
        },
      },
    },
  });
  return override;
};
export default MuiListItem;
