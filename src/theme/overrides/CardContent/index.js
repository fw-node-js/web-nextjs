import { createTheme } from "@mui/material";

const MuiCardContent = ({settings,theme}) => {
  const override = createTheme({
    components: {
      MuiCardContent: {
        styleOverrides: {
          root: { backgroundColor: "#DDD" },
        },
      },
    },
  });
  return override;
};
export default MuiCardContent;
