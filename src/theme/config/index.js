const themeConfig = {
    appName: process.env.NEXT_PUBLIC_APPNAME,
    mode: process.env.NEXT_PUBLIC_MODE,
    contentWidth: process.env.NEXT_PUBLIC_CONTENTWIDTH, 
    fontFamily: process.env.NEXT_PUBLIC_FONTFAMILY,
  };
  export default themeConfig;
  