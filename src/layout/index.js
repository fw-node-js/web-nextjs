import dynamic from "next/dynamic";

const TopNavLayout = dynamic(import("./TopNav"), { ssr: false });
const layouts = { TopNavLayout };
const Layout = ({ type, children }) => {
  const Wrapper =
    typeof type === "string"
      ? layouts[type]
      : ({ children }) => <>{children}</>;
  return <Wrapper sx={{ display: "flex" }}>{children}</Wrapper>;
};
export default Layout;
