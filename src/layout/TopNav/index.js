import AppBarComponent from "@/components/AppBar";
import { Box } from "@mui/material";
import styled from "styled-components";

const LayoutWrapper = styled(Box)(({ theme }) => ({
  height: "100vh",
}));

const TopNavLayout = ({ children }) => {
  return (
    <LayoutWrapper className="layout-wrapper">
      <AppBarComponent />
      {children}
    </LayoutWrapper>
  );
};
export default TopNavLayout;
