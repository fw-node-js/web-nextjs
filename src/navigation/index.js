import {
    HomeOutlined ,
    InsertChartOutlined ,
  } from "@mui/icons-material";
  import GavelOutlinedIcon from '@mui/icons-material/GavelOutlined';
  const Navigation = [
      {
        label: "Home",
        route: "/",
        icon: HomeOutlined 
      },
      {
        label: "Blog",
        route: "/blog",
        icon: GavelOutlinedIcon 
      },
      {
        label: "Contact",
        route: "/contact",
        icon: InsertChartOutlined 
      },
  ];
  export default Navigation;
  