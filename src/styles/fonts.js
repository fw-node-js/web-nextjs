const Fonts = [
  {
    name: "Source Sans Pro",
    link: "https://fonts.googleapis.com/css?family=Source+Sans+Pro&display=swap",
  },
  {
    name: "Sarabun",
    link: "https://fonts.googleapis.com/css?family=Sarabun&display=swap",
  },
  {
    name: "Kanit",
    link: "https://fonts.googleapis.com/css?family=Kanit&display=swap",
  },
  {
    name: "Prompt",
    link: "https://fonts.googleapis.com/css?family=Prompt&display=swap",
  },
  {
    name: "Mali",
    link: "https://fonts.googleapis.com/css?family=Mali&display=swap",
  },
  {
    name: "Itim",
    link: "https://fonts.googleapis.com/css?family=Itim&display=swap",
  },
];

export default Fonts;
