export const setLocalSettings = (settings) => {
  try {
    const json = JSON.stringify(settings);
    localStorage?.setItem("settings", json);
  } catch (e) {}
};

export const getLocalSettings = () => {
  try {
    const settings = localStorage.getItem("settings");
    return JSON.parse(settings);
  } catch (e) {}
};

export const removeSettings = () => {
  localStorage.removeItem("settings");
};
