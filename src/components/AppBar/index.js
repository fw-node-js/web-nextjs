import Navigation from "@/navigation";
import { AppBar, Box, Toolbar, Typography } from "@mui/material";
import NavItem from "./NavItem";

const AppBarComponent = ({ settings, saveSettings }) => {
  return (
    <Box>
      <AppBar position="static" elevation={0}>
        <Toolbar>
          <Box>
            <Typography
              variant="h6"
              sx={{
                ml: 1,
                mr: 1,
                lineHeight: 1,
                fontWeight: "bold",
                // textTransform: "uppercase",
                fontSize: "1.8rem !important",
              }}
            >
              {process.env.NEXT_PUBLIC_APPNAME}
            </Typography>
          </Box>
          <Box justifyContent="flex-end" sx={{ display: "flex" }}>
            {Navigation.map((item, index) => (
              <NavItem key={index} {...item} />
            ))}
          </Box>
        </Toolbar>
      </AppBar>
    </Box>
  );
};

export default AppBarComponent;
