import { useRouter } from "next/router";
import Link from "next/link";
import {
  ListItem,
  ListItemButton,
  ListItemText,
  Typography,
} from "@mui/material";
import { QuestionMarkOutlined } from "@mui/icons-material";

const NavLink = (props) => {
  let router = useRouter();
  let location = router.route;
  let NavIcon = props.icon;
  if (props.icon) {
    NavIcon = props.icon;
  } else {
    NavIcon = QuestionMarkOutlined;
  }
  return (
    <ListItem selected={props.route == location}>
      <Link href={props.route}>
        <ListItemButton sx={{ margin: 0, padding: 0 }}>
          {/* <NavIcon /> */}
          <ListItemText primary={<Typography>{props.label}</Typography>} />
        </ListItemButton>
      </Link>
    </ListItem>
  );
};
export default NavLink;
