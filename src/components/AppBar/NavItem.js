import NavLink from "./NavLink";
const NavItem = (props) => {
  return <NavLink {...props} />;
};
export default NavItem;
