import { Box, Card, CardContent, Grid, Typography } from "@mui/material";

const App = (props) => {
  const { settings } = props;
  return (
    <>
      <Grid container spacing={1}>
        <Grid item>
          <Card>
            <CardContent>
              <Typography>{settings.appName}</Typography>
            </CardContent>
          </Card>
        </Grid>
      </Grid>
    </>
  );
};
App.layout = "TopNavLayout";
export default App;
