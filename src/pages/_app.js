// import "@/styles/globals.css";
import Layout from "@/layout";
import Theme from "@/theme";
import dynamic from "next/dynamic";
import Head from "next/head";
import { useState } from "react";
import { QueryClient, QueryClientProvider } from "react-query";
import { SettingsApp, SettingsProvider } from "../context/settingsContext";

const MyApp = ({ Component, pageProps }) => {
  const [queryClient] = useState(() => new QueryClient());
  return (
    <>
      <Head>
        <link rel="icon" href="/favicon.ico" />
        <title>{process.env.NEXT_PUBLIC_APPNAME}</title>
      </Head>
      <QueryClientProvider client={queryClient}>
        <SettingsProvider>
          <SettingsApp>
            {({ settings }) => {
              return (
                <Theme settings={settings}>
                  <Layout type={Component.layout}>
                    <Component {...pageProps} settings={settings} />
                  </Layout>
                </Theme>
              );
            }}
          </SettingsApp>
        </SettingsProvider>
      </QueryClientProvider>
    </>
  );
};
export default dynamic(() => Promise.resolve(MyApp), {
  ssr: false,
});
