import Document, { Html, Head, Main, NextScript } from "next/document";
import Fonts from "@/styles/fonts";
class MyDocument extends Document {
  render() {
    return (
      <Html lang="en">
        <Head>
          {Fonts.map((item, key) => (
            <link key={key} href={item.link} rel="stylesheet" />
          ))}
        </Head>
        <body>
          <Main />
          <NextScript />
        </body>
      </Html>
    );
  }
}
export default MyDocument;
