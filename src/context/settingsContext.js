import { getLocalSettings, setLocalSettings } from "@/helper/settingStorage";
import themeConfig from "@/theme/config";
import { createContext, useState } from "react";

const initialSettings = {
  appName: themeConfig.appName,
  appTheme: themeConfig.appTheme,
  mode: themeConfig.mode,
  contentWidth: themeConfig.contentWidth,
  fontFamily: themeConfig.fontFamily,
  navStyle: themeConfig.navStyle,
};

const settings = getLocalSettings();

if (settings == null) {
  setLocalSettings(initialSettings);
}

export const SettingsContext = createContext({
  saveSettings: () => null,
  settings: getLocalSettings(),
});

export const SettingsProvider = ({ children }) => {
  const [settings, setSettings] = useState({ ...getLocalSettings() });
  const saveSettings = (updatedSettings) => {
    setLocalSettings(updatedSettings);
    setSettings(getLocalSettings());
  };

  return (
    <SettingsContext.Provider value={{ settings, saveSettings }}>
      {children}
    </SettingsContext.Provider>
  );
};

export const SettingsApp = SettingsContext.Consumer;
